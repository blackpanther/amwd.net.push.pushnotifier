# PushNotifier

Implements the [PushNotifier](https://pushnotifier.de) API reference.    
This library is written in .NET Standard 1.1.

[![NuGet](https://img.shields.io/nuget/v/AMWD.Net.Push.PushNotifier.svg?logo=nuget&label=-&colorA=white&style=popout-square)](https://www.nuget.org/packages/AMWD.Net.Push.PushNotifier)

## License

[MIT license](https://am-wd.de/?p=about#license)
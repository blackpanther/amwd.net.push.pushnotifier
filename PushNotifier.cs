﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AMWD.Net.Push
{
	/// <summary>
	/// Implements the API reference of pushnotifier.de
	/// </summary>
	public class PushNotifier : IDisposable
	{
		#region Fields

		private static readonly string BaseUrl = "https://api.pushnotifier.de";
		private static readonly string Version = "v2";

		private HttpClient client;

		#endregion Fields

		#region Constructors

		/// <summary>
		/// Initializes a new instance of the <see cref="PushNotifier"/> class.
		/// </summary>
		public PushNotifier()
		{
			client = new HttpClient
			{
				BaseAddress = new Uri(BaseUrl)
			};
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="PushNotifier"/> class.
		/// </summary>
		/// <param name="packageName">The package name to identify the application.</param>
		/// <param name="apiKey">The API key to authenticate the application.</param>
		/// <param name="token">User application token (optional).</param>
		public PushNotifier(string packageName, string apiKey, AppToken token = null)
			: this()
		{
			PackageName = packageName;
			ApiKey = apiKey;

			if (token != null)
			{
				Token = token;
				if (token.IsRenewRequired && !token.IsExpired)
				{
					RefreshToken().Wait();
				}
			}
		}

		#endregion Constructors

		#region Properties

		/// <summary>
		/// Gets or sets the package name to identify the application.
		/// </summary>
		public string PackageName { get; set; }

		/// <summary>
		/// Gets or sets the API key to authenticate the application.
		/// </summary>
		public string ApiKey { get; set; }

		/// <summary>
		/// Gets or sets the user application token to authenticate the user.
		/// </summary>
		public AppToken Token { get; set; }

		/// <summary>
		/// Gets or sets the priority of the message (default: <see cref="Priorities.Normal"/>).
		/// </summary>
		public Priorities Priority { get; set; } = Priorities.Normal;

		/// <summary>
		/// Gets or sets a list of available devices.
		/// </summary>
		public IEnumerable<Device> Devices { get; set; }

		/// <summary>
		/// Gets or sets a text message.
		/// </summary>
		public string Message { get; set; }

		/// <summary>
		/// Gets or sets a url.
		/// </summary>
		public string Url { get; set; }

		/// <summary>
		/// Gets or sets an image.
		/// </summary>
		public PnImage Image { get; set; }

		#endregion Properties

		#region Public methods

		/// <summary>
		/// Performs a login action for the user to receive the application token to authenticate with.
		/// </summary>
		/// <param name="username">The username.</param>
		/// <param name="password">The password.</param>
		/// <param name="apply">A value indicating whether the token should be applied to the current instance (default: <c>true</c>).</param>
		/// <param name="cancellationToken"></param>
		/// <returns>The application token.</returns>
		public async Task<AppToken> Login(string username, string password, bool apply = true, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(username))
			{
				throw new ArgumentNullException(nameof(username));
			}
			if (string.IsNullOrWhiteSpace(password))
			{
				throw new ArgumentNullException(nameof(password));
			}

			var content = new JObject
			{
				["username"] = username,
				["password"] = password
			};
			var response = await Request(RequestMethods.Post, "/user/login", null, content, cancellationToken);

			if (response.Status == HttpStatusCode.NotFound)
			{
				throw new ArgumentException($"User not found: {username}");
			}
			if (response.Status == HttpStatusCode.Forbidden)
			{
				throw new ArgumentException($"Login failed: {username}");
			}

			var token = new AppToken(response.Payload as JObject);
			if (apply)
			{
				Token = token;
			}

			return token;
		}

		/// <summary>
		/// Refreshes an application token before expiring.
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns>The new application token.</returns>
		public async Task<AppToken> RefreshToken(CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(Token?.Token))
			{
				throw new ArgumentNullException(nameof(Token));
			}

			var header = new Dictionary<string, string>
			{
				{ "X-AppToken", Token.Token }
			};
			var response = await Request(RequestMethods.Get, "/users/refresh", header, null, cancellationToken);

			if (response.Status == HttpStatusCode.NotFound)
			{
				throw new ArgumentException("User not found");
			}
			if (response.Status == HttpStatusCode.Forbidden)
			{
				throw new ArgumentException("Refresh failed");
			}

			Token = new AppToken(response.Payload as JObject);
			return Token;
		}

		/// <summary>
		/// Requests all available devices on the server.
		/// </summary>
		/// <param name="cancellationToken"></param>
		/// <returns></returns>
		public async Task<List<Device>> GetDevices(CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(Token?.Token))
			{
				throw new ArgumentNullException(nameof(Token));
			}

			var header = new Dictionary<string, string>
			{
				{ "X-AppToken", Token.Token }
			};
			var response = await Request(RequestMethods.Get, "/devices", header, null, cancellationToken);

			var list = new List<Device>();
			foreach (JObject el in (JArray)response.Payload)
			{
				list.Add(new Device(el));
			}

			return list;
		}

		#region Send Notifications

		/// <summary>
		/// Sends a text notification.
		/// </summary>
		/// <remarks>
		/// The text of the notification is displayed on the screen.
		/// </remarks>
		/// <param name="deviceIds">The device ids to send to.</param>
		/// <param name="cancellationToken"></param>
		/// <returns>Mapping of successful and failed devices.</returns>
		public async Task<Dictionary<string, bool>> SendText(IEnumerable<string> deviceIds = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(Message))
			{
				throw new ArgumentNullException(nameof(Message));
			}
			if (string.IsNullOrWhiteSpace(Token?.Token))
			{
				throw new ArgumentNullException(nameof(Token));
			}
			if (deviceIds?.Any() != true && Devices?.Any() != true)
			{
				throw new ArgumentNullException(nameof(deviceIds));
			}

			if (deviceIds?.Any() != true)
			{
				deviceIds = Devices.Select(d => d.Id);
			}

			var header = new Dictionary<string, string>
			{
				{ "X-AppToken", Token.Token }
			};
			var content = new JObject
			{
				["devices"] = JArray.FromObject(deviceIds),
				["content"] = Message,
				["silent"] = Priority < 0
			};
			var response = await Request(RequestMethods.Put, "/notifications/text", header, content, cancellationToken);

			return ProcessResponse(response);
		}

		/// <summary>
		/// Sends a url notification.
		/// </summary>
		/// <remarks>
		/// Tapping on the notification, the user is redirected to the link.
		/// </remarks>
		/// <param name="deviceIds">The device ids to send to.</param>
		/// <param name="cancellationToken"></param>
		/// <returns>Mapping of successful and failed devices.</returns>
		public async Task<Dictionary<string, bool>> SendUrl(IEnumerable<string> deviceIds = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(Url))
			{
				throw new ArgumentNullException(nameof(Url));
			}
			if (string.IsNullOrWhiteSpace(Token?.Token))
			{
				throw new ArgumentNullException(nameof(Token));
			}
			if (deviceIds?.Any() != true && Devices?.Any() != true)
			{
				throw new ArgumentNullException(nameof(deviceIds));
			}

			if (deviceIds?.Any() != true)
			{
				deviceIds = Devices.Select(d => d.Id);
			}

			var header = new Dictionary<string, string>
			{
				{ "X-AppToken", Token.Token }
			};
			var content = new JObject
			{
				["devices"] = JArray.FromObject(deviceIds),
				["url"] = Url,
				["silent"] = Priority < 0
			};
			var response = await Request(RequestMethods.Put, "/notifications/url", header, content, cancellationToken);

			return ProcessResponse(response);
		}

		/// <summary>
		/// Sends a notification.
		/// </summary>
		/// <remarks>
		/// The text is displayed on the screen.
		/// Tapping on the message will redirect the user to the url.
		/// </remarks>
		/// <param name="deviceIds">The device ids to send to.</param>
		/// <param name="cancellationToken"></param>
		/// <returns>Mapping of successful and failed devices.</returns>
		public async Task<Dictionary<string, bool>> SendNotification(IEnumerable<string> deviceIds = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (string.IsNullOrWhiteSpace(Message))
			{
				throw new ArgumentNullException(nameof(Message));
			}
			if (string.IsNullOrWhiteSpace(Url))
			{
				throw new ArgumentNullException(nameof(Url));
			}
			if (string.IsNullOrWhiteSpace(Token?.Token))
			{
				throw new ArgumentNullException(nameof(Token));
			}
			if (deviceIds?.Any() != true && Devices?.Any() != true)
			{
				throw new ArgumentNullException(nameof(deviceIds));
			}

			if (deviceIds?.Any() != true)
			{
				deviceIds = Devices.Select(d => d.Id);
			}

			var header = new Dictionary<string, string>
			{
				{ "X-AppToken", Token.Token }
			};
			var content = new JObject
			{
				["devices"] = JArray.FromObject(deviceIds),
				["content"] = Message,
				["url"] = Url,
				["silent"] = Priority < 0
			};
			var response = await Request(RequestMethods.Put, "/notifications/notification", header, content, cancellationToken);

			return ProcessResponse(response);
		}

		/// <summary>
		/// Sends an image notification.
		/// </summary>
		/// <param name="deviceIds">The device ids to send to.</param>
		/// <param name="cancellationToken"></param>
		/// <returns>Mapping of successful and failed devices.</returns>
		public async Task<Dictionary<string, bool>> SendImage(IEnumerable<string> deviceIds = null, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (isDisposed)
			{
				throw new ObjectDisposedException(GetType().FullName);
			}
			if (Image == null)
			{
				throw new ArgumentNullException(nameof(Image));
			}
			if (Image.Bytes?.Length <= 0)
			{
				throw new ArgumentNullException(nameof(Image.Bytes));
			}
			if (Image.Bytes.Length >= 5 * 1024 * 1024)
			{
				throw new ArgumentOutOfRangeException(nameof(Image.Bytes));
			}
			if (string.IsNullOrWhiteSpace(Image.FileName))
			{
				throw new ArgumentNullException(nameof(Image.FileName));
			}
			if (string.IsNullOrWhiteSpace(Token?.Token))
			{
				throw new ArgumentNullException(nameof(Token));
			}
			if (deviceIds?.Any() != true && Devices?.Any() != true)
			{
				throw new ArgumentNullException(nameof(deviceIds));
			}

			if (deviceIds?.Any() != true)
			{
				deviceIds = Devices.Select(d => d.Id);
			}

			var header = new Dictionary<string, string>
			{
				{ "X-AppToken", Token.Token }
			};
			var content = new JObject
			{
				["devices"] = JArray.FromObject(deviceIds),
				["content"] = Convert.ToBase64String(Image.Bytes),
				["filename"] = Image.FileName,
				["silent"] = Priority < 0
			};
			var response = await Request(RequestMethods.Put, "/notifications/image", header, content, cancellationToken);

			return ProcessResponse(response);
		}

		#endregion Send Notifications

		#endregion Public methods

		#region Private methods

		private Dictionary<string, bool> ProcessResponse(Response response)
		{
			if (response.Status == HttpStatusCode.BadRequest)
			{
				throw new ArgumentException("Request was not valid");
			}
			if (response.Status == HttpStatusCode.RequestEntityTooLarge)
			{
				throw new ArgumentException("Payload too large");
			}
			if (response.Status == HttpStatusCode.NotFound)
			{
				throw new ArgumentException("No device found");
			}

			var result = new Dictionary<string, bool>();
			foreach (var deviceId in (JArray)response.Payload["success"])
			{
				result.Add(deviceId.ToString(), true);
			}
			foreach (var deviceId in (JArray)response.Payload["error"])
			{
				result.Add(deviceId.ToString(), false);
			}

			return result;
		}

		private async Task<Response> Request(RequestMethods method, string path, Dictionary<string, string> header, JToken content, CancellationToken cancellationToken = default(CancellationToken))
		{
			if (string.IsNullOrWhiteSpace(PackageName))
			{
				throw new ArgumentNullException(nameof(PackageName));
			}
			if (string.IsNullOrWhiteSpace(ApiKey))
			{
				throw new ArgumentNullException(nameof(ApiKey));
			}
			if (string.IsNullOrWhiteSpace(path))
			{
				throw new ArgumentNullException(nameof(path));
			}

			if (header == null)
			{
				header = new Dictionary<string, string>();
			}
			if (content == null)
			{
				content = new JObject();
			}

			header["Authorization"] = "Basic " + Convert.ToBase64String(Encoding.UTF8.GetBytes(PackageName + ":" + ApiKey));
			header["Accept"] = "application/json";

			var request = new HttpRequestMessage
			{
				RequestUri = new Uri($"{BaseUrl}/{Version}/{path.Trim('/')}")
			};

			var auth = Convert.ToBase64String(Encoding.UTF8.GetBytes(PackageName + ":" + ApiKey));
			foreach (var kvp in header)
			{
				request.Headers.Add(kvp.Key, kvp.Value);
			}

			switch (method)
			{
				case RequestMethods.Get:
					request.Method = HttpMethod.Get;
					break;
				case RequestMethods.Post:
					request.Method = HttpMethod.Post;
					request.Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json");
					break;
				case RequestMethods.Put:
					request.Method = HttpMethod.Put;
					request.Content = new StringContent(JsonConvert.SerializeObject(content), Encoding.UTF8, "application/json");
					break;
				default:
					throw new NotSupportedException($"Request method '{method}' not known");
			}
			var response = await client.SendAsync(request, cancellationToken);
			var json = await response.Content.ReadAsStringAsync();

			if (response.StatusCode == HttpStatusCode.InternalServerError)
			{
				throw new HttpRequestException("Unknown server error");
			}
			if (response.StatusCode == HttpStatusCode.Unauthorized)
			{
				throw new ArgumentException(nameof(Token) + " invalid");
			}

			return new Response
			{
				Status = response.StatusCode,
				Payload = JToken.Parse(json)
			};
		}

		#endregion Private methods

		#region IDisposable implementation

		private bool isDisposed;

		/// <summary>
		/// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
		/// </summary>
		public void Dispose()
		{
			Dispose(true);
		}

		private void Dispose(bool disposing)
		{
			if (isDisposed)
			{
				return;
			}

			isDisposed = true;

			client?.Dispose();
			client = null;
		}

		#endregion IDisposable implementation

		#region Enums

		/// <summary>
		/// Represents the possible priorities for an pushover message.
		/// </summary>
		public enum Priorities
		{
			/// <summary>
			/// Normal priority.
			/// </summary>
			Normal = 0,
			/// <summary>
			/// Low priority. Keeps the device silent.
			/// </summary>
			Low = -1
		}

		private enum RequestMethods
		{
			Get,
			Post,
			Put
		}

		#endregion Enums

		#region Classes

		/// <summary>
		/// Contains the authentication information of the user.
		/// </summary>
		public class AppToken
		{
			#region Constructors

			/// <summary>
			/// Initializes a new instance of the <see cref="PushNotifier.AppToken"/> class.
			/// </summary>
			public AppToken()
			{ }

			/// <summary>
			/// Initializes a new instance of the <see cref="PushNotifier.AppToken"/> class.
			/// </summary>
			/// <param name="json">JSON serialized information.</param>
			public AppToken(string json)
				: this(JObject.Parse(json))
			{ }

			/// <summary>
			/// Initializes a new instance of the <see cref="PushNotifier.AppToken"/> class.
			/// </summary>
			/// <param name="jObject">The information as JSON object.</param>
			public AppToken(JObject jObject)
			{
				Token = jObject["app_token"].ToString();
				ExpiryUnix = jObject["expires_at"].Value<int>();
			}

			#endregion Constructors

			#region Properties

			/// <summary>
			/// Gets or sets the application token.
			/// </summary>
			[JsonProperty("app_token")]
			public string Token { get; set; }

			/// <summary>
			/// Gets or sets the time when the <see cref="Token"/> expires.
			/// </summary>
			[JsonIgnore]
			public DateTime ExpiresAt
			{
				get
				{
					return new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc)
						.AddSeconds(ExpiryUnix);
				}
				set
				{
					DateTime dt;
					switch (value.Kind)
					{
						case DateTimeKind.Utc: dt = value; break;
						case DateTimeKind.Local: dt = value.ToUniversalTime(); break;
						default: dt = DateTime.SpecifyKind(value, DateTimeKind.Utc); break;
					}
					ExpiryUnix = (int)dt.Subtract(new DateTime(1970, 1, 1, 0, 0, 0, 0, DateTimeKind.Utc))
						.TotalSeconds;
				}
			}

			/// <summary>
			/// Gets or sets the time when the <see cref="Token"/> expires.
			/// </summary>
			[JsonProperty("expires_at")]
			public int ExpiryUnix { get; set; }

			/// <summary>
			/// Gets a value indicating whether the token is expired.
			/// </summary>
			[JsonIgnore]
			public bool IsExpired => ExpiresAt <= DateTime.UtcNow;

			/// <summary>
			/// Gets a value indicating whether the token is about to expire and should be refreshed.
			/// </summary>
			[JsonIgnore]
			public bool IsRenewRequired => ExpiresAt.Subtract(DateTime.UtcNow) <= TimeSpan.FromDays(2);

			#endregion Properties

			#region Public methods

			/// <summary>
			/// Serializes the instance to a JSON string.
			/// </summary>
			/// <returns></returns>
			public string ToJson()
			{
				return JsonConvert.SerializeObject(this);
			}

			#endregion Public methods
		}

		/// <summary>
		/// Contains the device information.
		/// </summary>
		public class Device
		{
			#region Constructors

			/// <summary>
			/// Initializes a new instance of the <see cref="PushNotifier.Device"/> class.
			/// </summary>
			public Device()
			{ }

			/// <summary>
			/// Initializes a new instance of the <see cref="PushNotifier.Device"/> class.
			/// </summary>
			/// <param name="json">JSON serialized information.</param>
			public Device(string json)
				: this(JObject.Parse(json))
			{ }

			/// <summary>
			/// Initializes a new instance of the <see cref="PushNotifier.Device"/> class.
			/// </summary>
			/// <param name="jObject">Information as JSON object.</param>
			public Device(JObject jObject)
			{
				Id = jObject["id"].ToString();
				Name = jObject["title"].ToString();
				Model = jObject["model"].ToString();
				ImageUrl = jObject["image"].ToString();
			}

			#endregion Constructors

			#region Properties

			/// <summary>
			/// Gets or sets the device id.
			/// </summary>
			[JsonProperty("id")]
			public string Id { get; set; }

			/// <summary>
			/// Gets or sets the name.
			/// </summary>
			[JsonProperty("title")]
			public string Name { get; set; }

			/// <summary>
			/// Gets or sets the model type.
			/// </summary>
			[JsonProperty("model")]
			public string Model { get; set; }

			/// <summary>
			/// Gets or sets the image url.
			/// </summary>
			[JsonProperty("image")]
			public string ImageUrl { get; set; }

			#endregion Properties

			#region Public methods

			/// <summary>
			/// Serializes the instance to a JSON string.
			/// </summary>
			/// <returns></returns>
			public string ToJson()
			{
				return JsonConvert.SerializeObject(this);
			}

			#endregion Public methods
		}

		/// <summary>
		/// Contains the image content.
		/// </summary>
		public class PnImage
		{
			/// <summary>
			/// Gets or sets the file name.
			/// </summary>
			public string FileName { get; set; }

			/// <summary>
			/// Gets or sets the image content.
			/// </summary>
			public byte[] Bytes { get; set; }
		}

		private class Response
		{
			public HttpStatusCode Status { get; set; }

			public JToken Payload { get; set; }
		}

		#endregion Classes
	}
}
